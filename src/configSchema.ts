import * as Joi from 'joi';

export default Joi.object({
  API_KEY: Joi.string().required(),
  PORT: Joi.number().optional(),
  DB_DRIVER: Joi.string().valid('pgsql', 'mysql').required(),
  DATABASE_NAME: Joi.string().optional(),
  DATABASE_PORT: Joi.number().optional(),
  POSTGRES_USER: Joi.when('DB_DRIVER', {
    is: Joi.string().valid('pgsql'),
    then: Joi.string().required(),
    otherwise: Joi.string().optional(),
  }),
  POSTGRES_PASSWORD: Joi.when('DB_DRIVER', {
    is: Joi.string().valid('pgsql'),
    then: Joi.string().required(),
    otherwise: Joi.string().optional(),
  }),
  POSTGRES_HOST: Joi.when('DB_DRIVER', {
    is: Joi.string().valid('pgsql'),
    then: Joi.string().required(),
    otherwise: Joi.string().optional(),
  }),
  POSTGRES_DB: Joi.when('DB_DRIVER', {
    is: Joi.string().valid('pgsql'),
    then: Joi.string().required(),
    otherwise: Joi.string().optional(),
  }),
  POSTGRES_PORT: Joi.when('DB_DRIVER', {
    is: Joi.string().valid('pgsql'),
    then: Joi.number().required(),
    otherwise: Joi.number().optional(),
  }),
  MYSQL_USER: Joi.when('DB_DRIVER', {
    is: Joi.string().valid('mysql'),
    then: Joi.string().required(),
    otherwise: Joi.string().optional(),
  }),
  MYSQL_PASSWORD: Joi.when('DB_DRIVER', {
    is: Joi.string().valid('mysql'),
    then: Joi.string().required(),
    otherwise: Joi.string().optional(),
  }),
  MYSQL_HOST: Joi.when('DB_DRIVER', {
    is: Joi.string().valid('mysql'),
    then: Joi.string().required(),
    otherwise: Joi.string().optional(),
  }),
  MYSQL_DB: Joi.when('DB_DRIVER', {
    is: Joi.string().valid('mysql'),
    then: Joi.string().required(),
    otherwise: Joi.string().optional(),
  }),
  MYSQL_PORT: Joi.when('DB_DRIVER', {
    is: Joi.string().valid('mysql'),
    then: Joi.number().required(),
    otherwise: Joi.number().optional(),
  }),
});

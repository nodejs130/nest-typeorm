import { DataSource } from 'typeorm';
import { config } from 'dotenv';

import { DB_DRIVER } from './config';

config();

const dbDriver = process.env.DB_DRIVER ?? 'pgsql';
const type: DB_DRIVER = DB_DRIVER[dbDriver];
const username = process.env[`${type.toUpperCase()}_USER`];
const password = process.env[`${type.toUpperCase()}_PASSWORD`];
const host = process.env[`${type.toUpperCase()}_HOST`];
const database = process.env[`${type.toUpperCase()}_DB`];
const port = parseInt(process.env[`${type.toUpperCase()}_PORT`], 10);

export const connect = new DataSource({
  type,
  host,
  username,
  port,
  password,
  database,
  synchronize: false,
  logging: true,
  entities: ['src/**/*.entity.ts'],
  migrations: ['src/database/migrations/*.ts'],
});

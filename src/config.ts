import { registerAs } from '@nestjs/config';

export enum DB_DRIVER {
  pgsql = 'postgres',
  mysql = 'mysql',
}

export default registerAs('config', () => {
  return {
    database: {
      name: process.env.DATABASE_NAME,
      port: process.env.PORT,
    },
    dbDriver: DB_DRIVER[process.env.DB_DRIVER],
    postgres: {
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      host: process.env.POSTGRES_HOST,
      database: process.env.POSTGRES_DB,
      port: parseInt(process.env.POSTGRES_PORT, 10),
    },
    mysql: {
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      host: process.env.MYSQL_HOST,
      database: process.env.MYSQL_DB,
      port: parseInt(process.env.MYSQL_PORT, 10),
    },
    apiKey: process.env.API_KEY,
  };
});

import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import {
  CreateOrderItemDto,
  UpdateOrderItemDto,
} from 'src/users/dtos/order-item.dto';
import { Order } from 'src/users/entities/order.entity';
import { Product } from 'src/products/entities/product.entity';
import { OrderItem } from 'src/users/entities/order-item.entity';

@Injectable()
export class OrderItemService {
  constructor(
    @InjectRepository(OrderItem) private itemsRepo: Repository<OrderItem>,
    @InjectRepository(Order) private orderRepo: Repository<Order>,
    @InjectRepository(Product) private productRepo: Repository<Product>,
  ) {}

  async findOne(id: number) {
    const item = await this.itemsRepo.findOne({ where: { id } });
    if (!item) {
      throw new NotFoundException('Order item not found');
    }
    return item;
  }

  async create(payload: CreateOrderItemDto) {
    const order = await this.orderRepo.findOne({
      where: { id: payload.orderId },
    });
    const product = await this.productRepo.findOne({
      where: { id: payload.productId },
    });
    const item = new OrderItem();
    item.order = order;
    item.product = product;
    item.quantity = payload.quantity;
    return this.itemsRepo.save(item);
  }

  async update(id: number, payload: UpdateOrderItemDto) {
    const item = await this.findOne(id);
    this.itemsRepo.merge(item, payload);
    return this.itemsRepo.save(item);
  }

  remove(id: number) {
    return this.itemsRepo.delete(id);
  }
}
